import React from 'react';
import { RouterProvider } from 'react-router-dom';

import './App.css';
import { routes } from './routes';
import { Toast } from 'react-bootstrap';
import { Provider } from 'react-redux';
import store from './store';

interface State {
    errMessage?: string;
}

export default class App extends React.Component<any, State> {
    constructor(props: any) {
        super(props);
        window.onunhandledrejection = (evt) => {
            this.handleError(evt.reason);
        };
    }

    handleError(err: any) {
        this.setState({ errMessage: err.message });
    }

    componentDidCatch(error: any) {
        this.handleError(error);
    }

    render(): React.ReactNode {
        return (
            <Provider store={store}>
                <RouterProvider router={routes} />

                <div className="toast-area">
                    <Toast
                        show={!!this.state?.errMessage}
                        onClose={() => this.setState({ errMessage: '' })}
                        autohide
                        bg="danger">
                        <Toast.Header className="justify-content-between custom-toast-header text-danger">
                            Error: {this.state?.errMessage}
                        </Toast.Header>
                    </Toast>
                </div>
            </Provider>
        );
    }
}
