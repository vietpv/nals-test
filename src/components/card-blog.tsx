import { Link, useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import { Dropdown } from 'react-bootstrap';
import { Edit, Eye, MoreHorizontal, Trash2 } from 'react-feather';
import { BlogDto } from '../dto/blog';

const Wrapper = styled.div`
    .blog-box {
        display: flex;
        align-items: flex-start;
        justify-content: flex-start;
        border-bottom: 1px dashed #ced4da;
        padding: 1rem 2.5rem 1rem 0;
        position: relative;
        &-img {
            position: relative;
            margin-right: 1rem;
            a {
                width: 6rem;
                padding-top: 100%;
                display: block;
            }
            img {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                object-fit: cover;
                border-radius: 0.5rem;
            }
        }
        &-body {
            width: calc(100% - 7rem);
            &--link {
                color: var(--bs-gray-900);
                text-decoration: none;
                &:hover {
                    color: var(--bs-primary);
                }
            }
            &--title {
                font-size: 1.125rem;
                font-weight: bold;
                overflow: hidden;
                display: -webkit-box;
                -webkit-line-clamp: 2;
                -webkit-box-orient: vertical;
            }
            &--desc {
                font-size: 0.875rem;
                margin-bottom: 0;
                color: var(--bs-gray-600);
                overflow: hidden;
                display: -webkit-box;
                -webkit-line-clamp: 3;
                -webkit-box-orient: vertical;
            }

            &--action {
                position: absolute;
                top: 50%;
                right: 0;
                transform: translate(0, -50%);
                .dropstart {
                    .dropdown-toggle {
                        width: 2rem;
                        height: 2rem;
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        border-radius: 50%;
                        padding: 0;
                        &:before,
                        &:after {
                            display: none;
                        }
                    }
                    .dropdown-item {
                        display: flex;
                        align-items: center;
                        font-size: 0.875rem;
                        gap: 0.5rem;
                    }
                }
            }
        }
    }
`;
interface Props {
    data: BlogDto;
    onEdit: (id: string) => void;
    onDelete: (id: string) => void;
}

export default function CardBlog({ data, onEdit, onDelete }: Props) {
    const navigate = useNavigate();
    return (
        <Wrapper>
            <div className="blog-box">
                <div className="blog-box-img">
                    <Link to={`blog/${data.id}`}>
                        <img src={data.image} alt={data.title} />
                    </Link>
                </div>
                <div className="blog-box-body">
                    <Link to={`blog/${data.id}`} className="blog-box-body--link">
                        <h4 className="mt-0 mb-1 blog-box-body--title">{data.title}</h4>
                    </Link>
                    <p className="blog-box-body--desc">{data.content.substring(0, 300)}...</p>
                </div>
                <div className="blog-box-body--action">
                    <Dropdown drop="start">
                        <Dropdown.Toggle variant="light">
                            <MoreHorizontal size={20} />
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item onClick={() => navigate(`blog/${data.id}`)}>
                                <Eye size={16} /> Detail
                            </Dropdown.Item>
                            <Dropdown.Item className="text-info" onClick={() => onEdit(data.id)}>
                                <Edit size={16} /> Edit
                            </Dropdown.Item>
                            <Dropdown.Item
                                className="text-danger"
                                onClick={() => onDelete(data.id)}>
                                <Trash2 size={16} /> Delete
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
        </Wrapper>
    );
}
