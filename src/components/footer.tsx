import { Link } from 'react-router-dom';
import styled from 'styled-components';

const Wrapper = styled.footer`
    box-shadow: 5px -3px 5px 0px rgba(0, 0, 0, 0.15);
`;
export default function Footer() {
    return (
        <Wrapper className="bg-white">
            <div className="container">
                <div className="py-3">
                    <ul className="nav justify-content-center border-bottom pb-3 mb-3">
                        <li className="nav-item">
                            <Link to="#" className="nav-link px-2 text-body-secondary">
                                Home
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="#" className="nav-link px-2 text-body-secondary">
                                Features
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="#" className="nav-link px-2 text-body-secondary">
                                Pricing
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="#" className="nav-link px-2 text-body-secondary">
                                FAQs
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="#" className="nav-link px-2 text-body-secondary">
                                About
                            </Link>
                        </li>
                    </ul>
                    <p className="text-center text-body-secondary">
                        Copyright © 2023 — NAL Solutions. All Rights Reserved
                    </p>
                </div>
            </div>
        </Wrapper>
    );
}
