import styled from 'styled-components';
import Logo from './logo';

const Wrapper = styled.header`
    box-shadow: 0px 3px 5px rgba(0, 0, 0, 0.1);
`;
export default function Header() {
    return (
        <Wrapper className="p-3 bg-white border-bottom">
            <div className="container">
                <div className="d-flex flex-wrap align-items-center justify-content-between">
                    <Logo />
                    <div className="dropdown text-end">
                        <img
                            src="https://lh3.googleusercontent.com/ogw/AAEL6shFuqai-SupTRKEGVbo7I1UN3oEXFumnDF17ExC7qA=s32-c-mo"
                            alt="mdo"
                            width="32"
                            height="32"
                            className="rounded-circle"
                        />
                    </div>
                </div>
            </div>
        </Wrapper>
    );
}
