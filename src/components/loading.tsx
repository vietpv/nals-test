import { Spinner } from 'react-bootstrap';

export default function Loading() {
    return (
        <div className="text-center w-100 loading-box">
            <Spinner animation="border" role="status">
                <span className="visually-hidden">Loading...</span>
            </Spinner>
        </div>
    );
}
