import { Link } from 'react-router-dom';
import LogoImg from '../assets/images/logo.png';

interface Props {
    path?: string;
}

export default function Logo({ path }: Props) {
    return (
        <Link to={path || ''} className="logo">
            <img src={LogoImg} alt="logo" height={40} />
        </Link>
    );
}
