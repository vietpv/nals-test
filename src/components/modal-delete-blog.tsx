import { useState } from 'react';
import { Button, Modal, Toast } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { deleteBlog } from '../store/actions/blog';

interface Props {
    show: boolean;
    blogId?: string;
    onClose: () => void;
}

export default function ModalDeleteBlog({ show, blogId, onClose }: Props) {
    const [isSuccess, setIsSuccess] = useState(false);
    const dispatch = useDispatch();
    const { isLoadingDetail } = useSelector((store: RootState) => store.blogSlice);

    const handleDeleteBlog = async () => {
        if (blogId) {
            const result = await deleteBlog(+blogId, dispatch);
            setIsSuccess(result);
            result && onClose();
        }
    };

    return (
        <>
            <Modal show={show} onHide={onClose}>
                <Modal.Header closeButton>Delete blog #{blogId}</Modal.Header>
                <Modal.Body>Do you want to delete this blog (id: #{blogId})</Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" type="button" onClick={onClose}>
                        Cancel
                    </Button>
                    <Button
                        disabled={isLoadingDetail}
                        variant="primary"
                        type="button"
                        onClick={handleDeleteBlog}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>
            <div className="toast-area">
                <Toast show={isSuccess} onClose={() => setIsSuccess(false)} autohide bg="success">
                    <Toast.Header className="justify-content-between custom-toast-header text-success">
                        Deleted successfully
                    </Toast.Header>
                </Toast>
            </div>
        </>
    );
}
