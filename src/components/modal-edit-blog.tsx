import { useCallback, useEffect, useState } from 'react';
import { Button, Form, Modal, Toast } from 'react-bootstrap';
import { createBlog, fetchBlogDetail, updateBlog } from '../store/actions/blog';
import { RootState } from '../store';
import { useDispatch, useSelector } from 'react-redux';
import Loading from './loading';
import { BlogDto } from '../dto/blog';

interface Props {
    show: boolean;
    mode?: 'edit' | 'create';
    blogId?: string;
    onClose: () => void;
}

export default function ModalEditBlog({ show, mode = 'create', blogId, onClose }: Props) {
    const dispatch = useDispatch();
    const { isLoadingDetail } = useSelector((store: RootState) => store.blogSlice);
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [image, setImage] = useState('');
    const [isSuccess, setIsSuccess] = useState(false);
    const isDisabled = !title || !content || !image;

    const handleGetBlog = useCallback(async () => {
        if (blogId) {
            const result = await fetchBlogDetail(+blogId, dispatch);
            if (mode === 'edit') {
                setTitle(result.title);
                setContent(result.content);
                setImage(result.image);
            } else {
                resetForm();
            }
        }
    }, [blogId, dispatch, mode]);

    const resetForm = () => {
        setTitle('');
        setContent('');
        setImage('');
    };

    const handleUpdateBlog = async () => {
        if (blogId) {
            const payload: Partial<BlogDto> = {
                id: blogId,
                title,
                content,
                image,
            };
            const result = await updateBlog(payload, dispatch);
            setIsSuccess(result);
            result && handleClose();
        }
    };

    const handleClose = () => {
        resetForm();
        onClose();
    };

    const hanldeCreateBlog = async () => {
        const payload: Partial<BlogDto> = {
            title,
            content,
            image,
        };

        const result = await createBlog(payload, dispatch);
        setIsSuccess(result);
        result && handleClose();
    };

    useEffect(() => {
        handleGetBlog();
    }, [handleGetBlog]);

    return (
        <>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    {mode === 'create' ? 'Create Blog' : 'Edit Blog'}
                </Modal.Header>
                <Modal.Body>
                    <Form className="postition-relative">
                        <Form.Group className="mb-3" controlId="blogImage">
                            <Form.Label>Image</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="Enter image link"
                                value={image}
                                onChange={(e) => setImage(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="blogImage">
                            <Form.Label>Title</Form.Label>
                            <Form.Control
                                required
                                type="text"
                                placeholder="Enter title"
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                            />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="blogContent">
                            <Form.Label>Content</Form.Label>
                            <Form.Control
                                required
                                as="textarea"
                                placeholder="Enter content"
                                rows={7}
                                value={content}
                                onChange={(e) => setContent(e.target.value)}
                            />
                        </Form.Group>
                        {isLoadingDetail && <Loading />}
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" type="button" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button
                        disabled={isLoadingDetail || isDisabled}
                        variant="primary"
                        type="button"
                        onClick={mode === 'edit' ? handleUpdateBlog : hanldeCreateBlog}>
                        Submit
                    </Button>
                </Modal.Footer>
            </Modal>
            <div className="toast-area">
                <Toast show={isSuccess} onClose={() => setIsSuccess(false)} autohide bg="success">
                    <Toast.Header className="justify-content-between custom-toast-header text-success">
                        Updated successfully
                    </Toast.Header>
                </Toast>
            </div>
        </>
    );
}
