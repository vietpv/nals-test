import { Pagination } from 'react-bootstrap';

interface Props {
    active: number;
    numberOfPages: number;
    onClickPage: (page: number) => void;
}
export default function CustomPagination({ active, numberOfPages, onClickPage }: Props) {
    let items = [];
    for (let number = 1; number <= numberOfPages; number++) {
        items.push(
            <Pagination.Item
                onClick={() => onClickPage(number)}
                key={number}
                disabled={number === active}
                active={number === active}>
                {number}
            </Pagination.Item>
        );
    }

    return <Pagination className="justify-content-center">{items}</Pagination>;
}
