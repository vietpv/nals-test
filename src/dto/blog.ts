export interface BlogDto {
    id: string;
    createdAt: Date;
    title: string;
    image: string;
    content: string;
}

export type OrderType = 'asc' | 'desc';
export type SortByType = 'id' | 'title' | 'createdAt' | 'content';
