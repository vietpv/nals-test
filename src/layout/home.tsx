import { Outlet } from 'react-router-dom';
import Header from '../components/header';
import Footer from '../components/footer';

export default function LayoutHome() {
    return (
        <div className="layout-home">
            <Header />
            <div className="main-container">
                <Outlet />
            </div>
            <Footer />
        </div>
    );
}
