import styled from 'styled-components';
import { Calendar } from 'react-feather';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { useParams } from 'react-router-dom';
import { fetchBlogDetail } from '../store/actions/blog';
import { useCallback, useEffect } from 'react';
import Loading from '../components/loading';
import moment from 'moment';

const Wrapper = styled.div`
    background-color: #f8f9fa;
    .blog-detail {
        max-width: 800px;
        margin: 0 auto;
        &--header {
            margin-bottom: 1rem;
            text-align: center;
        }
        &--title {
            font-size: 1.5rem;
            font-weight: bold;
            margin: 0 0 0.5rem;
        }
        &--meta {
            font-size: 1rem;
            color: var(--bs-gray-600);
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 0.5rem;
        }
        &--content {
            box-shadow: 0 10px 20px rgba(137, 137, 137, 0.1);
            border-radius: 1rem 1rem 0 0;
            padding: 1.5rem 1.5rem 3rem 1.5rem;
        }
        &--thumb {
            width: 100%;
            max-width: 500px;
            margin: 0 auto 2rem auto;
            text-align: center;
            img {
                max-width: 100%;
                border-radius: 1rem;
            }
        }
    }
`;

export default function BlogDetail() {
    const dispatch = useDispatch();
    const { blogDetail, isLoadingDetail } = useSelector((store: RootState) => store.blogSlice);
    const { id } = useParams();
    const handleGetDetail = useCallback(async () => {
        if (id) {
            await fetchBlogDetail(+id, dispatch);
        }
    }, [id, dispatch]);

    useEffect(() => {
        handleGetDetail();
    }, [handleGetDetail]);

    return (
        <Wrapper>
            <div className="blog-detail">
                <div className="container">
                    {isLoadingDetail ? (
                        <Loading />
                    ) : (
                        blogDetail && (
                            <>
                                <div className="py-3 blog-detail--header">
                                    <h1 className="blog-detail--title mt-0">{blogDetail.title}</h1>
                                    <div className="blog-detail--meta">
                                        <Calendar size={16} />{' '}
                                        {moment(blogDetail.createdAt).format('YYYY-MM-DD HH:mm')}
                                    </div>
                                </div>
                                <div className="blog-detail--content bg-white">
                                    <div className="blog-detail--thumb">
                                        <img src={blogDetail.image} alt={blogDetail.title} />
                                    </div>
                                    <div className="entry-content">
                                        <p>{blogDetail.content}</p>
                                    </div>
                                </div>
                            </>
                        )
                    )}
                </div>
            </div>
        </Wrapper>
    );
}
