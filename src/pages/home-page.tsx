import styled from 'styled-components';
import _ from 'lodash';
import CardBlog from '../components/card-blog';
import { Button, Form, InputGroup } from 'react-bootstrap';
import { useCallback, useEffect, useState } from 'react';
import { OrderType, SortByType } from '../dto/blog';
import Loading from '../components/loading';
import ModalEditBlog from '../components/modal-edit-blog';
import { useDispatch, useSelector } from 'react-redux';
import { fetchBlogs } from '../store/actions/blog';
import { RootState } from '../store';
import CustomPagination from '../components/pagination';
import ModalDeleteBlog from '../components/modal-delete-blog';

const Wrapper = styled.div`
    min-height: calc(100vh - 72px - 145px);
    .blog-box {
    }
`;
export default function Homepage() {
    const [modeEdit, setModeEdit] = useState<'edit' | 'create'>('create');
    const [showModalEdit, setShowModalEdit] = useState(false);
    const [showModalDelete, setShowModalDelete] = useState(false);
    const [search, setSearch] = useState('');
    const [page, setPage] = useState(1);
    const [pages, setPages] = useState(0);
    const limit = 10;
    const [sortBy, setSortBy] = useState<SortByType>('id');
    const [order, setOrder] = useState<OrderType>('asc');
    const [idSelectedBlog, setIdSelectedBlog] = useState<string>();
    const dispatch = useDispatch();
    const { blog, isLoading } = useSelector((store: RootState) => store.blogSlice);

    const setSearchDebounce = _.debounce(setSearch, 500);

    const handleGetBlog = useCallback(
        async (page: number, sortBy: SortByType, order: OrderType, search: string) => {
            const payload = {
                page,
                limit,
                sortBy,
                order,
                search,
            };

            const result = await fetchBlogs(payload, dispatch);
            setPages(result?.total / limit);
        },
        [dispatch]
    );

    const onChangePage = (page: number) => {
        setPage(page);
    };

    const handleCreate = () => {
        setModeEdit('create');
        setShowModalEdit(true);
    };

    const handleEdit = (id: string) => {
        setModeEdit('edit');
        setIdSelectedBlog(id);
        setShowModalEdit(true);
    };

    const handleDelete = async (id: string) => {
        setIdSelectedBlog(id);
        setShowModalDelete(true);
    };

    useEffect(() => {
        setPage(1);
    }, [search]);

    useEffect(() => {
        handleGetBlog(page, sortBy, order, search);
    }, [handleGetBlog, page, sortBy, order, search]);

    return (
        <Wrapper className="bg-white">
            <div className="container">
                <div className="filter-box py-4">
                    <div className="row">
                        <div className="col-12 col-xs-12 col-sm-12 col-md-6 mb-2">
                            <InputGroup className="input-search">
                                <Form.Control
                                    placeholder="Enter keyword"
                                    onChange={(e) => setSearchDebounce(e.target.value)}
                                />
                            </InputGroup>
                        </div>
                        <div className="col-12 col-xs-12 col-sm-12 col-md-6">
                            <div className="row">
                                <div className="col-6 mb-2">
                                    <Form.Select
                                        onChange={(e) => {
                                            setOrder(e.target.value as OrderType);
                                        }}>
                                        <option>Order by</option>
                                        <option value="asc">Ascending</option>
                                        <option value="desc">Descending</option>
                                    </Form.Select>
                                </div>
                                <div className="col-6 mb-2">
                                    <Form.Select
                                        onChange={(e) => {
                                            setSortBy(e.target.value as SortByType);
                                        }}>
                                        <option>Sort by</option>
                                        <option value="id">By Id</option>
                                        <option value="title">By Title</option>
                                        <option value="createdAt">By Date</option>
                                        <option value="content">By Content</option>
                                    </Form.Select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="d-flex justify-content-between">
                    <h3 className="font-weight-bold">List blog</h3>
                    <Button variant="primary" onClick={handleCreate}>
                        Create a new blog
                    </Button>
                </div>
                <div className="blog-box pt-4 position-relative">
                    {blog.length ? (
                        <>
                            {blog.map((blog) => (
                                <CardBlog
                                    key={blog.id}
                                    data={blog}
                                    onEdit={handleEdit}
                                    onDelete={handleDelete}
                                />
                            ))}
                            <div className="py-4 text-center">
                                <CustomPagination
                                    onClickPage={onChangePage}
                                    active={page}
                                    numberOfPages={pages}
                                />
                            </div>
                        </>
                    ) : (
                        !isLoading && <div className="py-3">No blog found</div>
                    )}
                    {isLoading && <Loading />}
                </div>
            </div>
            <ModalEditBlog
                show={showModalEdit}
                mode={modeEdit}
                onClose={() => setShowModalEdit(false)}
                blogId={idSelectedBlog}
            />
            <ModalDeleteBlog
                show={showModalDelete}
                blogId={idSelectedBlog}
                onClose={() => setShowModalDelete(false)}
            />
        </Wrapper>
    );
}
