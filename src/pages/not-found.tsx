import { useNavigate } from 'react-router-dom';
import Logo from '../components/logo';

export default function NotFound() {
    const navigate = useNavigate();
    return (
        <div className="page-not-found">
            <div className="container">
                <div className="px-4 py-5 my-5 text-center">
                    <div className="d-block mx-auto mb-4">
                        <Logo />
                    </div>
                    <h1 className="display-5 fw-bold text-body-emphasis">
                        Oops! That page can’t be found.
                    </h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4">
                            It looks like nothing was found at this location. Maybe try one of the
                            links below or a search?
                        </p>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                            <button
                                onClick={() => navigate('')}
                                type="button"
                                className="btn btn-primary btn-lg px-4 gap-3">
                                Home
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
