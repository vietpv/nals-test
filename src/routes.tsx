import { createBrowserRouter } from 'react-router-dom';
import LayoutHome from './layout/home';
import BlogDetail from './pages/blog-detail';
import Homepage from './pages/home-page';
import NotFound from './pages/not-found';

export const routes = createBrowserRouter([
    {
        path: '',
        element: <LayoutHome />,
        children: [
            {
                path: '',
                element: <Homepage />,
            },
            {
                path: 'blog/:id',
                element: <BlogDetail />,
            },
        ],
    },
    {
        path: '*',
        element: <NotFound />,
    },
]);
