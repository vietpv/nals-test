import { AppDispatch } from '..';
import {
    createBlogApi,
    deleteBlogDetailApi,
    getBlogDetailApi,
    getBlogsApi,
    updateBlogDetailApi,
} from '../api/blog';
import {
    setIsLoading,
    setBlogs,
    setBlogDetail,
    setIsLoadingDetail,
    updateBlogStore,
    deleteBlogStore,
    createBlogStore,
} from '../slice/blog';

const fetchBlogs = async (payload: any, dispatch: AppDispatch) => {
    const { page, limit } = payload;
    const newPayload = {
        order: payload.order,
        sortBy: payload.sortBy,
        search: payload.search,
    };

    try {
        dispatch(setIsLoading(true));
        const result = await getBlogsApi(newPayload);
        const data = result.slice((page - 1) * limit, page * limit);
        dispatch(setBlogs(data));
        // because api doesn't return total param i have to handle like this
        const response = {
            total: result.length,
            page,
            limit,
        };
        return response;
    } finally {
        dispatch(setIsLoading(false));
    }
};

const fetchBlogDetail = async (id: number, dispatch: AppDispatch) => {
    try {
        dispatch(setIsLoadingDetail(true));
        const result = await getBlogDetailApi(id);
        dispatch(setBlogDetail(result));
        return result;
    } finally {
        dispatch(setIsLoadingDetail(false));
    }
};

const createBlog = async (payload: any, dispatch: AppDispatch) => {
    try {
        dispatch(setIsLoadingDetail(true));
        const result = await createBlogApi(payload);
        dispatch(createBlogStore(result));
        return true;
    } finally {
        dispatch(setIsLoadingDetail(false));
    }
};

const updateBlog = async (payload: any, dispatch: AppDispatch) => {
    try {
        const { id, ...data } = payload;
        dispatch(setIsLoadingDetail(true));
        const result = await updateBlogDetailApi(+id, data);
        dispatch(updateBlogStore(result));
        return true;
    } finally {
        dispatch(setIsLoadingDetail(false));
    }
};

const deleteBlog = async (id: number, dispatch: AppDispatch) => {
    try {
        dispatch(setIsLoadingDetail(true));
        const result = await deleteBlogDetailApi(id);
        dispatch(setBlogDetail(result));
        dispatch(deleteBlogStore(result));
        return true;
    } finally {
        dispatch(setIsLoadingDetail(false));
    }
};

export { fetchBlogs, fetchBlogDetail, updateBlog, deleteBlog, createBlog };
