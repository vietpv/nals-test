import axios from 'axios';
import queryString from 'query-string';

/** base url to make requests to the movie database */
// const instance = axios.create({
//     baseURL: 'https://5f55a98f39221c00167fb11a.mockapi.io',
// });

// export default instance;

const callApi = async (method: any, url: string, payload?: any, data?: any) => {
    const query = queryString.stringify(payload);
    const baseURL = `https://5f55a98f39221c00167fb11a.mockapi.io${url}?${query}`;

    try {
        const response = await axios({
            method,
            baseURL,
            data,
        });
        return response.data;
    } catch (error: any) {
        throw new Error(error.response.data);
    }
};

export default callApi;
