import callApi from './axios-config';

export const getBlogsApi = async (payload?: any) => {
    const response = await callApi('GET', '/blogs', payload);

    return response;
};

export const getBlogDetailApi = async (id: number) => {
    const response = await callApi('GET', `/blogs/${id}`);
    return response;
};

export const updateBlogDetailApi = async (id: number, body: any) => {
    const response = await callApi('PUT', `/blogs/${id}`, undefined, body);
    return response;
};

export const deleteBlogDetailApi = async (id: number) => {
    const response = await callApi('DELETE', `/blogs/${id}`);
    return response;
};

export const createBlogApi = async (data: any) => {
    const response = await callApi('POST', `/blogs`, undefined, data);
    return response;
};
