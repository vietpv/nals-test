import { configureStore } from '@reduxjs/toolkit';
import blogSlice from './slice/blog';

const store = configureStore({
    reducer: {
        blogSlice,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
