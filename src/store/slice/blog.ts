import { createSlice } from '@reduxjs/toolkit';
import { BlogDto } from '../../dto/blog';

const authSlice = createSlice({
    name: 'blog',
    initialState: {
        blog: [] as BlogDto[],
        blogDetail: {} as BlogDto,
        isLoading: false,
        isLoadingDetail: false,
    },
    reducers: {
        setBlogs: (state, action) => {
            state.blog = action.payload;
        },
        setBlogDetail: (state, action) => {
            state.blogDetail = action.payload;
        },
        createBlogStore: (state, action) => {
            state.blog = [action.payload, ...state.blog];
        },
        updateBlogStore: (state, action) => {
            const index = state.blog.findIndex((b) => b.id === action.payload.id);
            state.blog = [
                ...state.blog.slice(0, index),
                { ...state.blog[index], ...action.payload },
                ...state.blog.slice(index + 1),
            ];
        },
        deleteBlogStore: (state, action) => {
            state.blog = state.blog.filter((b) => b.id !== action.payload.id);
        },
        setIsLoading: (state, action) => {
            state.isLoading = action.payload;
        },
        setIsLoadingDetail: (state, action) => {
            state.isLoadingDetail = action.payload;
        },
    },
});

const { reducer, actions } = authSlice;

export const {
    setBlogs,
    setBlogDetail,
    setIsLoading,
    setIsLoadingDetail,
    createBlogStore,
    updateBlogStore,
    deleteBlogStore,
} = actions;

export default reducer;
